<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 20.07.2017
  Time: 20:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form method="post" action="">
    <table>
        <tr>
            <td colspan="2">
                <c:if test="${errors != null}">
                    <ul>
                        <c:forEach var="error" items="${errors}">
                            <li>${error}</li>
                        </c:forEach>
                    </ul>
                </c:if>

            </td>
        </tr>
        <tr>
            <td>Login</td>
            <td>
                <input type="text" name="login"/>
            </td>
        </tr>
        <tr>
            <td>Password</td>
            <td>
                <input type="password" name="password"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="submit" value="LOGIN"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
