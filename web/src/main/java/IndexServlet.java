import dao.imp.UserDaoImp;
import model.Users;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serg on 20.07.2017.
 */
@WebServlet(name = "IndexServlet", urlPatterns = "/")
public class IndexServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        List<String> errors = new ArrayList<String>();


        if (login.isEmpty()) {
            errors.add("Login cannot be empty");
        }

        if (password.isEmpty()) {
            errors.add("Password cannot be empty");
        }

        if (!errors.isEmpty()) {
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request, response);
        } else {

            Users user = new UserDaoImp().getByUsername(login);
            if (user != null) {
                if (user.getPassword().equals(password)) {
                //authenticate user
                    HttpSession session = request.getSession(true);
                    session.setAttribute("user", user);
                    response.addCookie(new Cookie("sid", session.getId()));
                    response.sendRedirect("home");

                } else {
                    errors.add("User not found");
                    request.setAttribute("errors", errors);
                    request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request, response);
                }
            } else {
                errors.add("User not found");
                request.setAttribute("errors", errors);
                request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request, response);
            }



        }



    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request,response);

    }
}
