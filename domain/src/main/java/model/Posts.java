package model;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Serg on 11.07.2017.
 */
@Entity
@Table(name = "posts", schema = "blog")
public class Posts {
    private int id;
    private int idUser;
    private String post;
    private Collection<Comments> commentsesById;
    private Users usersByIdUser;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_user", nullable = false)
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "post", nullable = false, length = 500)
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @OneToMany(mappedBy = "postsByIdPost", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Comments> getCommentsesById() {
        return commentsesById;
    }

    public void setCommentsesById(Collection<Comments> commentsesById) {
        this.commentsesById = commentsesById;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id", nullable = false)
    public Users getUsersByIdUser() {
        return usersByIdUser;
    }

    public void setUsersByIdUser(Users usersByIdUser) {
        this.usersByIdUser = usersByIdUser;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "post_media",
            joinColumns = @JoinColumn(name = "id_post", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_media", referencedColumnName = "id")
    )
    private List<Media> mediaList;

    public List<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Posts posts = (Posts) o;

        if (id != posts.id) return false;
        if (idUser != posts.idUser) return false;
        if (post != null ? !post.equals(posts.post) : posts.post != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idUser;
        result = 31 * result + (post != null ? post.hashCode() : 0);
        return result;
    }

}
