package model;

import javax.persistence.*;

/**
 * Created by Serg on 11.07.2017.
 */
@Entity
@Table(name = "comments", schema = "blog")
public class Comments {
    private int id;
    private int idPost;
    private String comment;
    private Posts postsByIdPost;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_post", nullable = false)
    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    @Basic
    @Column(name = "comment", nullable = false, length = 255)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_post", referencedColumnName = "id", nullable = false)
    public Posts getPostsByIdPost() {
        return postsByIdPost;
    }

    public void setPostsByIdPost(Posts postsByIdPost) {
        this.postsByIdPost = postsByIdPost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comments comments = (Comments) o;

        if (id != comments.id) return false;
        if (idPost != comments.idPost) return false;
        if (comment != null ? !comment.equals(comments.comment) : comments.comment != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idPost;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }


}
