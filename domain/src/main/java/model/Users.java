package model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Serg on 11.07.2017.
 */
@Entity
@Table(name = "users")
public class Users {

    private String firstname;
    private String lastname;
    private int id;

    private String login;
    private String password;


    private Collection<Posts> postsesById;
    private Collection<Media> mediasById;

    @Basic
    @Column(name = "firstname", nullable = false, length = 32)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "lastname", nullable = false, length = 32)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "login", nullable = false, length = 32)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 32)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(mappedBy = "usersByIdUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Posts> getPostsesById() {
        return postsesById;
    }

    public void setPostsesById(Collection<Posts> postsesById) {
        this.postsesById = postsesById;
    }

    @OneToMany(mappedBy = "usersByIdUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Media> getMediasById() {
        return mediasById;
    }

    public void setMediasById(Collection<Media> mediasById) {
        this.mediasById = mediasById;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (id != users.id) return false;
        if (firstname != null ? !firstname.equals(users.firstname) : users.firstname != null) return false;
        if (lastname != null ? !lastname.equals(users.lastname) : users.lastname != null) return false;
        if (login != null ? !login.equals(users.login) : users.login != null) return false;
        return password != null ? password.equals(users.password) : users.password == null;

    }

    @Override
    public int hashCode() {
        int result = firstname != null ? firstname.hashCode() : 0;
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + id;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
