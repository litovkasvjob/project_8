package model;

import javax.persistence.*;

/**
 * Created by Serg on 11.07.2017.
 */
@Entity
@Table(name = "post_media", schema = "blog", catalog = "")
public class PostMedia {
    private int id;
    private Integer idPost;
    private Integer idMedia;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_post", nullable = true)
    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    @Basic
    @Column(name = "id_media", nullable = true)
    public Integer getIdMedia() {
        return idMedia;
    }

    public void setIdMedia(Integer idMedia) {
        this.idMedia = idMedia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostMedia postMedia = (PostMedia) o;

        if (id != postMedia.id) return false;
        if (idPost != null ? !idPost.equals(postMedia.idPost) : postMedia.idPost != null) return false;
        if (idMedia != null ? !idMedia.equals(postMedia.idMedia) : postMedia.idMedia != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (idPost != null ? idPost.hashCode() : 0);
        result = 31 * result + (idMedia != null ? idMedia.hashCode() : 0);
        return result;
    }
}
