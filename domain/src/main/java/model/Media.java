package model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Serg on 11.07.2017.
 */
@Entity
@Table(name = "media", schema = "blog")
public class Media {
    private int id;
    private int idUser;
    private String media;
    private Users usersByIdUser;
    private MediaType mediaType;
    private Date dateCreated;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_user", nullable = false)
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "media", nullable = false, length = 45)
    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id", nullable = false)
    public Users getUsersByIdUser() {
        return usersByIdUser;
    }

    public void setUsersByIdUser(Users usersByIdUser) {
        this.usersByIdUser = usersByIdUser;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "post_media",
            joinColumns = @JoinColumn(name = "id_media", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_post", referencedColumnName = "id")
    )
    private List<Posts> postsList;

    public List<Posts> getPostsList() {
        return postsList;
    }

    public void setPostsList(List<Posts> postsList) {
        this.postsList = postsList;
    }

    @Basic
    @Column(name = "media_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    @Basic
    @Column(name = "date_time", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date getDateCreated() {

        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Media media1 = (Media) o;

        if (id != media1.id) return false;
        if (idUser != media1.idUser) return false;
        if (media != null ? !media.equals(media1.media) : media1.media != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idUser;
        result = 31 * result + (media != null ? media.hashCode() : 0);
        return result;
    }

    public static enum MediaType{
        VIDEO, AUDIO, IMAGE
    }

}
