package service;

import dao.app.Dao;
import dao.app.UsersDao;
import dao.imp.UserDaoImp;
import model.Posts;
import model.Users;

/**
 * Created by Serg on 11.07.2017.
 */
public class UsersServise {
    private UsersDao usersDao;
    Dao<Posts, Integer> posts;

    public UsersServise() {
        usersDao = new UserDaoImp();
    }

    public UsersServise(UsersDao usersDao, Dao<Posts, Integer> posts) {
        this.usersDao = usersDao;
        this.posts = posts;
    }

    Users getById(Integer id) {
        return usersDao.getById(id);
    }
}
