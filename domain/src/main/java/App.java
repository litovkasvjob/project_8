import dao.HibernateUtils;
import dao.app.UsersDao;
import dao.imp.UserDaoImp;
import model.Users;

public class App {
    public static void main(String[] args) {
        UsersDao uDao = new UserDaoImp();
        System.out.println(uDao.getList());
        Users user = new Users();
        user.setFirstname("vasja");
        user.setLastname("pupkin");
        uDao.create(user);
        System.out.println(uDao.getList());
        Users vasja = uDao.getById(1);
        System.out.println(vasja);
        System.out.println("posts : ");
        System.out.println(vasja.getPostsesById());
        HibernateUtils.getSession().close();
    }
}
