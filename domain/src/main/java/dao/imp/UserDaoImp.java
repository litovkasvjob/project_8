package dao.imp;

import dao.HibernateUtils;
import dao.app.AbstractDao;
import dao.app.UsersDao;
import model.Users;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Serg on 11.07.2017.
 */
public class UserDaoImp extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public Users getByUsername(String login) {
        Criteria criteria = getSession().createCriteria(Users.class)
                .add(Restrictions.eq("login", login));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

    @Override
    public Users getById(Integer key) {
        Session session = HibernateUtils.getSession();
        Users u = (Users)session.get(Users.class, key);
        return u;
    }

    @Override
    public Users create(Users o) {
        Session session = HibernateUtils.getSession();
        Transaction t = session.beginTransaction();
        try{
            session.save(o);
            t.commit();
        }catch (Exception e){
            t.rollback();
            throw new RuntimeException("Cannot save user", e);
        }
        return o;
    }

    @Override
    public boolean update(Users o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean remove(Users o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Users> getList() {
        Session session = HibernateUtils.getSession();
        return session.createCriteria(Users.class).list();
    }



}
