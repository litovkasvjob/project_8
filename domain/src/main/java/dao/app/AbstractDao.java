package dao.app;


import dao.HibernateUtils;
import org.hibernate.Session;


import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Serg on 11.07.2017.
 */
public abstract class AbstractDao<T, I extends Serializable> implements Dao<T, I> {

    private final Class<T> entityClass;

    public Session getSession() {
        return HibernateUtils.getSession();
    }

    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public T getById(I key) {
        Session session = getSession();
        return (T) session.load(entityClass, key);
    }

    @Override
    public T create(T o) {

        Session session = getSession();

        return (T) session.save(o);
    }

    @Override
    public boolean update(T o) {
        Session session = getSession();
        session.update(o);
        return true;
    }

    @Override
    public boolean remove(T o) {
        Session session = getSession();
        session.delete(o);
        return true;
    }

    @Override
    public List<T> getList() {
        Session session = getSession();
        return (List<T>) session.createCriteria(entityClass).list();
    }
}




