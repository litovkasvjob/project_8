package dao.app;

import java.util.List;

/**
 * Created by Serg on 11.07.2017.
 */
public interface Dao<T, I> {
    T getById(I key);

    T create(T o);

    boolean update(T o);

    boolean remove(T o);

    List<T> getList();

}
