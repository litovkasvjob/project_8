package dao.app;

import model.Users;

/**
 * Created by Serg on 11.07.2017.
 */
public interface UsersDao extends Dao<Users, Integer> {

    Users getByUsername(String username);

}
